## Instalacion de las dependencias

```bash
$ npm install
```
## Configuracion del proyecto
```bash
$ cp .env.example .env
```

## Dockerizar la aplicacion para la BD
```bash
$ docker compose up -d
```

## Comando para correr la aplicacion

```bash
$ npm run start:dev
```

## Comando para realizar los test
``` bash
$ npm run test -- --verbose
```

## Comandos personalizados para los test por separado
``` bash
# usuario
$ npm run test:usuario -- --verbose

# autenticarce
$ npm run test:auth -- --verbose
```