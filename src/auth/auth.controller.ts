import { Body, Controller, Post } from '@nestjs/common';
import { VerificarDto } from 'src/usuario/dto/verificar.dto';
import { UsuarioService } from 'src/usuario/usuario.service';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private usuarioService: UsuarioService
    ){}

    @Post('login')
    async login(@Body() verificarDto: VerificarDto) {
        const usuario = await this.usuarioService.verificarCuenta(verificarDto);
        return await this.authService.login(usuario);
    }
}
