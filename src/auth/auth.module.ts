import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from 'src/entities';
import { JwtModule } from '@nestjs/jwt';
import { UsuarioService } from 'src/usuario/usuario.service';
import { UsuarioRepository } from 'src/usuario/repository/usuario.repository';
import { JwtStrategy, JwtauthGuard } from './guards/index.guard';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [ 
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SEED,
      signOptions: { expiresIn: '6h' },
    }),
    TypeOrmModule.forFeature([Usuario]),
    UsuarioModule
   ],
  providers: [AuthService, UsuarioService, UsuarioRepository, JwtStrategy, JwtauthGuard],
  controllers: [AuthController]
})
export class AuthModule {}
