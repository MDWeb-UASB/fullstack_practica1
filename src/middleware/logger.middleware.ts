import { Injectable, Logger, NestMiddleware } from "@nestjs/common";
import { NextFunction } from "express";

@Injectable()
export class LoggerMiddleware implements NestMiddleware{
    private readonly logger = new Logger(LoggerMiddleware.name)
    use(req: Request, res: Response, next: NextFunction){
        this.logger.log('Esta es la Solicitud: ', req.method, req.url);
        this.logger.log('Esta es la Respuesta: ', res);
        next();
    }
}