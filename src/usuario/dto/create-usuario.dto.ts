import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CreateUsuarioDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(5, { 
        message: 'El nombre de usuario debe tener al menos 5 caracteres'})
    @IsAlphanumeric(null, { message: 'Solo se permiten numeros y letras' })
    nombreUsuario: string;
    @IsString()
    @IsEmail(null, { message: 'Ingrese un correo valido' })
    @IsNotEmpty()
    email: string;
    @IsString()
    @IsNotEmpty()
    @MinLength(8, { message: 'La contrasela al menos debe tener 8 caracteres'})
    password: string;
}
