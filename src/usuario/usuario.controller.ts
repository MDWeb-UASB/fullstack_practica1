import { 
  Controller, 
  Get, 
  Post, 
  Body, 
  Patch, 
  Param, 
  Delete, 
  UseGuards} from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { 
  CreateUsuarioDto, 
  UpdateUsuarioDto,
  VerificarDto } from './dto/index.dto';
import { JwtStrategy, JwtauthGuard } from 'src/auth/guards/index.guard';

// @UseGuards(JwtStrategy, JwtauthGuard)
@Controller('usuarios')
export class UsuarioController {
  constructor(private readonly usuarioService: UsuarioService) {}
  
  @Post()
  create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return this.usuarioService.create(createUsuarioDto);
  }
  
  @Get()
  findAll() {
    return this.usuarioService.findAll();
  }
  
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usuarioService.findOne(+id);
  }

  @Get('username/:nombreUsuario')
  findByUserName(@Param('nombreUsuario') nombreUsuario: string) {
    return this.usuarioService.findByUserName(nombreUsuario);
  }

  @Get('email/:email')
  findByEmail(@Param('email') email: string) {
    return this.usuarioService.findByEmail(email);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUsuarioDto: UpdateUsuarioDto) {
    return this.usuarioService.update(+id, updateUsuarioDto);
  }
  
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usuarioService.remove(+id);
  }

  @Post('verificar')
  verificar(@Body() verificarDto: VerificarDto) {
    console.log("Revisar: ",verificarDto);
    return this.usuarioService.verificarCuenta(verificarDto);
  }
}
