import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioService } from './usuario.service';
import { UsuarioRepository } from './repository/usuario.repository';
import { CreateUsuarioDto } from './dto/index.dto';
import { Usuario } from './entities/usuario.entity';

describe('UsuarioService', () => {
  let service: UsuarioService;
  let usuarioRepository: UsuarioRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsuarioService,
        {
          provide: UsuarioRepository,
          useValue: {
            findByUserName: jest.fn(),
            findByEmail: jest.fn(),
            crear: jest.fn(),
            listar: jest.fn(),
            findById: jest.fn(),
            actualizar: jest.fn(),
            eliminar: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<UsuarioService>(UsuarioService);
    usuarioRepository = module.get<UsuarioRepository>(UsuarioRepository);
  });

  it('Deberia de estar', () => {
    expect(service).toBeDefined();
  });

  it('Deberia de buscar por nombre de Usuario', async () => {
    const nombreUsuario = 'testuser';
    const usuarioMock: Usuario = { id: 1, nombreUsuario, email: 'testuser@test.com', password: '123456' };

    (usuarioRepository.findByUserName as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await service.findByUserName(nombreUsuario);

    expect(result).toEqual(usuarioMock);
    expect(usuarioRepository.findByUserName).toHaveBeenCalledWith(nombreUsuario);
  });

  it('Deberia de buscar por email', async () => {
    const email = 'testuser@test.com';
    const usuarioMock: Usuario = { id: 1, nombreUsuario: 'testuser', email, password: '123456' };

    (usuarioRepository.findByEmail as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await service.findByEmail(email);

    expect(result).toEqual(usuarioMock);
    expect(usuarioRepository.findByEmail).toHaveBeenCalledWith(email);
  });

  it('Deberia de crear un usuario', async () => {
    const createUsuarioDto: CreateUsuarioDto = {
      nombreUsuario: 'testuser',
      password: 'password',
      email: 'test@test.com'
    };

    (usuarioRepository.findByUserName as jest.Mock).mockResolvedValue(null);

    const usuarioMock: Usuario = {
      id: 1,
      nombreUsuario: createUsuarioDto.nombreUsuario,
      email: createUsuarioDto.email,
      password: createUsuarioDto.password
    };

    (usuarioRepository.crear as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await service.create(createUsuarioDto);

    expect(result).toEqual(usuarioMock);
  });

  it('Deberai de listar a los usuarios', async () => {
    const usuariosMock: Usuario[] = [
      { id: 1, nombreUsuario: 'user1', email: 'user1@test.com', password: '123456' },
      { id: 2, nombreUsuario: 'user2', email: 'user2@test.com', password: '123456' },
    ];

    (usuarioRepository.listar as jest.Mock).mockResolvedValue(usuariosMock);

    const result = await service.findAll();

    expect(result).toEqual(usuariosMock);
  });
  
  it('Deberia de encontrar a un usuario', async () => {
    const id = 1;
    const usuarioMock: Usuario = { id, nombreUsuario: 'testuser', email: 'testuser@test.com', password: '123456' };

    (usuarioRepository.findById as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await service.findOne(id);

    expect(result).toEqual(usuarioMock);
  });

  it('Deberia de actualizar a un usuario', async () => {
    const id = 1;
    const updateUsuarioDto = { nombreUsuario: 'updateduser', email: 'updated@test.com' };
    const usuarioMock: Usuario = { id, nombreUsuario: 'testuser', email: 'testuser@test.com', password: '123456' };

    (usuarioRepository.findById as jest.Mock).mockResolvedValue(usuarioMock);
    (usuarioRepository.actualizar as jest.Mock).mockResolvedValue({ ...usuarioMock, ...updateUsuarioDto });

    const result = await service.update(id, updateUsuarioDto);

    expect(result).toEqual({ ...usuarioMock, ...updateUsuarioDto });
  });

  it('Deberia de eliminar a un usuario', async () => {
    const id = 1;
    const usuarioMock: Usuario = { id, nombreUsuario: 'testuser', email: 'testuser@test.com', password: '123456' };

    (usuarioRepository.findById as jest.Mock).mockResolvedValue(usuarioMock);
    (usuarioRepository.eliminar as jest.Mock).mockResolvedValue({ affected: 1 });

    const result = await service.remove(id);

    expect(result).toEqual({ affected: 1 });
  });

});
