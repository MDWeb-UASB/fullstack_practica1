import { Injectable } from '@nestjs/common';
import { CreateUsuarioDto, UpdateUsuarioDto, VerificarDto } from './dto/index.dto';
import { UsuarioRepository } from './repository/usuario.repository';
import { Usuario } from './entities/usuario.entity';

@Injectable()
export class UsuarioService {
  constructor(
    private readonly usuarioRepository: UsuarioRepository
  ){}

  async create(createUsuarioDto: CreateUsuarioDto) {
    const validar = await this.usuarioRepository.findByUserName(createUsuarioDto.nombreUsuario);
    if(validar) { throw new Error(`El nombre usuario: ${createUsuarioDto.nombreUsuario} ya existe`)};
    return this.usuarioRepository.crear(createUsuarioDto);
  }

  findAll() {
    return this.usuarioRepository.listar();
  }

  findOne(id: number) {
    return this.usuarioRepository.findById(id);
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const validacion = this.usuarioRepository.findById(id);
    if(!validacion) {
      throw new Error(`Usuario con el id: ${id} no encontrado`);
    }
    const usuario = this.usuarioRepository.actualizar(id, updateUsuarioDto);
    return usuario;
  }

  remove(id: number) {
    return this.usuarioRepository.eliminar(id);
  }

  async verificarCuenta(verificarDto: VerificarDto): Promise<Usuario>{
    const validar = await this.usuarioRepository.findByEmail(verificarDto.email);
    if(!validar || validar.password !== verificarDto.password) { throw new Error(`El correro: ${verificarDto.email} o la contraseña: ${verificarDto.password} no son correctos!`); }
    return this.usuarioRepository.findByEmail(verificarDto.email);
  }

  async findByUserName(nombreUsuario: string): Promise<Usuario> {
    return this.usuarioRepository.findByUserName(nombreUsuario);
  }

  async findByEmail(email: string): Promise<Usuario> {
    return this.usuarioRepository.findByEmail(email);
  }
}
